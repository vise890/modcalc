# `modcalc`

A small, ridiculously over-engineered, utility for converting between Unix
permissions formats.

Mainly to get my feet wet with Rust.

## Resources

- [Unix Permissions](https://en.wikipedia.org/wiki/File_system_permissions)
- [Bitwise Operations](https://en.wikipedia.org/wiki/Bitwise_operation)

//! A small toy utility for converting between Unix permissions formats

#[derive(Debug, PartialEq)]
struct RWX {
    read: bool,
    write: bool,
    execute: bool,
}

#[derive(Debug, PartialEq)]
struct Permissions {
    owner: RWX,
    group: RWX,
    others: RWX,
}

trait ToNumeric {
    fn to_numeric(&self) -> u16;
}

trait ToSymbolic {
    fn to_symbolic(&self) -> String;
}

trait Read<T>
where
    Self: std::marker::Sized,
{
    fn read(T) -> Result<Self, &'static str>;
}

impl RWX {
    const READ: u16 = 1 << 2;
    const WRITE: u16 = 1 << 1;
    const EXECUTE: u16 = 1;
}

impl ToNumeric for RWX {
    fn to_numeric(&self) -> u16 {
        let mut n: u16 = 0;
        if self.read {
            n += RWX::READ;
        };
        if self.write {
            n += RWX::WRITE;
        };
        if self.execute {
            n += RWX::EXECUTE;
        };
        n
    }
}

impl ToNumeric for Permissions {
    fn to_numeric(&self) -> u16 {
        let owner = self.owner.to_numeric();
        let group = self.group.to_numeric();
        let others = self.others.to_numeric();
        owner << 6 | group << 3 | others
    }
}

impl ToSymbolic for RWX {
    fn to_symbolic(&self) -> String {
        let r = if self.read { "r" } else { "-" };
        let w = if self.write { "w" } else { "-" };
        let x = if self.execute { "x" } else { "-" };
        r.to_string() + w + x
    }
}

impl ToSymbolic for Permissions {
    fn to_symbolic(&self) -> String {
        let owner = self.owner.to_symbolic();
        let group = self.group.to_symbolic();
        let others = self.others.to_symbolic();
        owner + &group + &others
    }
}

impl Read<u16> for RWX {
    fn read(n: u16) -> Result<RWX, &'static str> {
        let is_set = |bit| (bit & n) != 0;
        if n > 0o7 {
            Err("Invalid range for RWX! (should be in [0-7])")
        } else {
            Ok(RWX {
                read: is_set(RWX::READ),
                write: is_set(RWX::WRITE),
                execute: is_set(RWX::EXECUTE),
            })
        }
    }
}

impl Read<u16> for Permissions {
    fn read(n: u16) -> Result<Permissions, &'static str> {
        if n > 0o777 {
            Err("Invalid range for Permissions! (should be in [0-777])")
        } else {
            let owner_bits = n >> 6;
            let group_bits = (n >> 3) & 0o07;
            let others_bits = n & 0o007;
            Ok(Permissions {
                owner: RWX::read(owner_bits)?,
                group: RWX::read(group_bits)?,
                others: RWX::read(others_bits)?,
            })
        }
    }
}

fn read_rwx_char(exp_char: char, c: char) -> Result<bool, &'static str> {
    match c {
        '-' => Ok(false),
        _ if c == exp_char => Ok(true),
        _ => Err("invalid char for RWX! should be in [r,w,x]"),
    }
}

impl Read<String> for RWX {
    fn read(s: String) -> Result<RWX, &'static str> {
        if s.len() != 3 {
            Err("invalid len for RWX")
        } else {
            // FIXME there's gotta be a better way!
            // (->> s
            //      (zip [\r \w \x])
            //      (map read_rwx_char))))
            //
            // case s of
            //   [r, w, x] => ...
            let mut r = false;
            let mut w = false;
            let mut x = false;
            let mut chars = s.chars();
            if let Some(c) = chars.next() {
                r = read_rwx_char('r', c)?
            }
            if let Some(c) = chars.next() {
                w = read_rwx_char('w', c)?
            }
            if let Some(c) = chars.next() {
                x = read_rwx_char('x', c)?
            }
            Ok(RWX {
                read: r,
                write: w,
                execute: x,
            })
        }
    }
}

impl<'a> Read<&'a str> for RWX {
    fn read(s: &'a str) -> Result<RWX, &'static str> {
        RWX::read(s.to_string())
    }
}

impl Read<String> for Permissions {
    fn read(s: String) -> Result<Permissions, &'static str> {
        if s.len() != 9 {
            Err("invalid length for Permissions")
        } else {
            Ok(Permissions {
                owner: RWX::read(&s[0..3])?,
                group: RWX::read(&s[3..6])?,
                others: RWX::read(&s[6..=8])?,
            })
        }
    }
}

impl<'a> Read<&'a str> for Permissions {
    fn read(s: &'a str) -> Result<Permissions, &'static str> {
        Permissions::read(s.to_string())
    }
}

fn main() {}

#[cfg(test)]
mod test {
    use super::*;

    const RWX_000: RWX = RWX {
        read: false,
        write: false,
        execute: false,
    };
    const RWX_00X: RWX = RWX {
        read: false,
        write: false,
        execute: true,
    };
    const RWX_0W0: RWX = RWX {
        read: false,
        write: true,
        execute: false,
    };
    const RWX_0WX: RWX = RWX {
        read: false,
        write: true,
        execute: true,
    };
    const RWX_R00: RWX = RWX {
        read: true,
        write: false,
        execute: false,
    };
    const RWX_R0X: RWX = RWX {
        read: true,
        write: false,
        execute: true,
    };
    const RWX_RW0: RWX = RWX {
        read: true,
        write: true,
        execute: false,
    };
    const RWX_RWX: RWX = RWX {
        read: true,
        write: true,
        execute: true,
    };

    const PERMS_000: Permissions = Permissions {
        owner: RWX_000,
        group: RWX_000,
        others: RWX_000,
    };
    const PERMS_111: Permissions = Permissions {
        owner: RWX_00X,
        group: RWX_00X,
        others: RWX_00X,
    };
    const PERMS_222: Permissions = Permissions {
        owner: RWX_0W0,
        group: RWX_0W0,
        others: RWX_0W0,
    };
    const PERMS_333: Permissions = Permissions {
        owner: RWX_0WX,
        group: RWX_0WX,
        others: RWX_0WX,
    };
    const PERMS_444: Permissions = Permissions {
        owner: RWX_R00,
        group: RWX_R00,
        others: RWX_R00,
    };
    const PERMS_555: Permissions = Permissions {
        owner: RWX_R0X,
        group: RWX_R0X,
        others: RWX_R0X,
    };
    const PERMS_666: Permissions = Permissions {
        owner: RWX_RW0,
        group: RWX_RW0,
        others: RWX_RW0,
    };
    const PERMS_700: Permissions = Permissions {
        owner: RWX_RWX,
        group: RWX_000,
        others: RWX_000,
    };
    const PERMS_740: Permissions = Permissions {
        owner: RWX_RWX,
        group: RWX_R00,
        others: RWX_000,
    };
    const PERMS_755: Permissions = Permissions {
        owner: RWX_RWX,
        group: RWX_R0X,
        others: RWX_R0X,
    };
    const PERMS_770: Permissions = Permissions {
        owner: RWX_RWX,
        group: RWX_RWX,
        others: RWX_000,
    };
    const PERMS_777: Permissions = Permissions {
        owner: RWX_RWX,
        group: RWX_RWX,
        others: RWX_RWX,
    };

    #[test]
    fn can_represent_rwx_in_symbolic_notation() {
        assert_eq!("---", RWX_000.to_symbolic());
        assert_eq!("--x", RWX_00X.to_symbolic());
        assert_eq!("-w-", RWX_0W0.to_symbolic());
        assert_eq!("-wx", RWX_0WX.to_symbolic());
        assert_eq!("r--", RWX_R00.to_symbolic());
        assert_eq!("r-x", RWX_R0X.to_symbolic());
        assert_eq!("rw-", RWX_RW0.to_symbolic());
        assert_eq!("rwx", RWX_RWX.to_symbolic());
    }

    #[test]
    fn can_represent_permissions_in_symbolic_notation() {
        assert_eq!("---------", PERMS_000.to_symbolic());
        assert_eq!("--x--x--x", PERMS_111.to_symbolic());
        assert_eq!("-w--w--w-", PERMS_222.to_symbolic());
        assert_eq!("-wx-wx-wx", PERMS_333.to_symbolic());
        assert_eq!("r--r--r--", PERMS_444.to_symbolic());
        assert_eq!("r-xr-xr-x", PERMS_555.to_symbolic());
        assert_eq!("rw-rw-rw-", PERMS_666.to_symbolic());
        assert_eq!("rwx------", PERMS_700.to_symbolic());
        assert_eq!("rwxr-----", PERMS_740.to_symbolic());
        assert_eq!("rwxr-xr-x", PERMS_755.to_symbolic());
        assert_eq!("rwxrwx---", PERMS_770.to_symbolic());
        assert_eq!("rwxrwxrwx", PERMS_777.to_symbolic());
    }

    #[test]
    fn can_represent_permissions_in_numeric_notation() {
        assert_eq!(0o000, PERMS_000.to_numeric());
        assert_eq!(0o111, PERMS_111.to_numeric());
        assert_eq!(0o222, PERMS_222.to_numeric());
        assert_eq!(0o333, PERMS_333.to_numeric());
        assert_eq!(0o444, PERMS_444.to_numeric());
        assert_eq!(0o555, PERMS_555.to_numeric());
        assert_eq!(0o666, PERMS_666.to_numeric());
        assert_eq!(0o700, PERMS_700.to_numeric());
        assert_eq!(0o740, PERMS_740.to_numeric());
        assert_eq!(0o755, PERMS_755.to_numeric());
        assert_eq!(0o770, PERMS_770.to_numeric());
        assert_eq!(0o777, PERMS_777.to_numeric());
    }

    #[test]
    fn can_read_rwx_from_number() {
        assert_eq!(RWX_000, RWX::read(0o0).unwrap());
        assert_eq!(RWX_00X, RWX::read(0o1).unwrap());
        assert_eq!(RWX_0W0, RWX::read(0o2).unwrap());
        assert_eq!(RWX_0WX, RWX::read(0o3).unwrap());
        assert_eq!(RWX_R00, RWX::read(0o4).unwrap());
        assert_eq!(RWX_R0X, RWX::read(0o5).unwrap());
        assert_eq!(RWX_RW0, RWX::read(0o6).unwrap());
        assert_eq!(RWX_RWX, RWX::read(0o7).unwrap());

        assert!(RWX::read(0o77).is_err());
    }

    #[test]
    fn can_read_permissions_from_number() {
        assert_eq!(PERMS_000, Permissions::read(0o000).unwrap());
        assert_eq!(PERMS_111, Permissions::read(0o111).unwrap());
        assert_eq!(PERMS_222, Permissions::read(0o222).unwrap());
        assert_eq!(PERMS_333, Permissions::read(0o333).unwrap());
        assert_eq!(PERMS_444, Permissions::read(0o444).unwrap());
        assert_eq!(PERMS_555, Permissions::read(0o555).unwrap());
        assert_eq!(PERMS_666, Permissions::read(0o666).unwrap());
        assert_eq!(PERMS_700, Permissions::read(0o700).unwrap());
        assert_eq!(PERMS_740, Permissions::read(0o740).unwrap());
        assert_eq!(PERMS_755, Permissions::read(0o755).unwrap());
        assert_eq!(PERMS_770, Permissions::read(0o770).unwrap());
        assert_eq!(PERMS_777, Permissions::read(0o777).unwrap());

        assert!(RWX::read(0o7771).is_err());
    }

    #[test]
    fn can_read_rwx_from_string() {
        assert_eq!(RWX_000, RWX::read("---").unwrap());
        assert_eq!(RWX_RWX, RWX::read("rwx").unwrap());
        assert_eq!(RWX_R0X, RWX::read("r-x").unwrap());

        assert!(RWX::read("nopety_nope").is_err());
        assert!(RWX::read("rww").is_err());
    }

    #[test]
    fn can_read_permissions_from_string() {
        assert_eq!(PERMS_000, Permissions::read("---------").unwrap());
        assert_eq!(PERMS_111, Permissions::read("--x--x--x").unwrap());
        assert_eq!(PERMS_222, Permissions::read("-w--w--w-").unwrap());
        assert_eq!(PERMS_333, Permissions::read("-wx-wx-wx").unwrap());
        assert_eq!(PERMS_444, Permissions::read("r--r--r--").unwrap());
        assert_eq!(PERMS_555, Permissions::read("r-xr-xr-x").unwrap());
        assert_eq!(PERMS_666, Permissions::read("rw-rw-rw-").unwrap());
        assert_eq!(PERMS_700, Permissions::read("rwx------").unwrap());
        assert_eq!(PERMS_740, Permissions::read("rwxr-----").unwrap());
        assert_eq!(PERMS_755, Permissions::read("rwxr-xr-x").unwrap());
        assert_eq!(PERMS_770, Permissions::read("rwxrwx---").unwrap());
        assert_eq!(PERMS_777, Permissions::read("rwxrwxrwx").unwrap());

        assert!(Permissions::read("nopety_nope").is_err());
        assert!(Permissions::read("rwXrwxrwx").is_err());
    }

    fn roundtrip_test(numeric_in: u16) -> () {
        let p = Permissions::read(numeric_in).unwrap();
        let symbolic = p.to_symbolic();
        let p2 = Permissions::read(symbolic).unwrap();
        let numeric_out = p2.to_numeric();
        assert_eq!(numeric_in, numeric_out)
    }

    #[test]
    fn can_roundtrip_permissions() {
        roundtrip_test(0o000);
        roundtrip_test(0o111);
        roundtrip_test(0o222);
        roundtrip_test(0o333);
        roundtrip_test(0o444);
        roundtrip_test(0o555);
        roundtrip_test(0o666);
        roundtrip_test(0o700);
        roundtrip_test(0o740);
        roundtrip_test(0o755);
        roundtrip_test(0o770);
        roundtrip_test(0o777);
    }
}
